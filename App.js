import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Apps from './index'
import { Provider } from 'react-redux'
import store from './components/store'

export default function App() {
  return (
    <Provider store={store}>
    <Apps />
    </Provider>
  );
}