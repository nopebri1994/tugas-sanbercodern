import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import LoginScreen from './components/LoginScreen';
import Exit from './components/exit';
import HomeScreen from './components/HomeScreen';
import Aboutscreen from './components/AboutScreen';
import DetailScreenDunia from './components/DetailScreenDunia';
import DetailScreenIndo from './components/DetailScreenIndo';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const Tabscreen = () => (
    <Tab.Navigator initialRouteName="Home">
      <Tab.Screen name="Tentang Kami" component={Aboutscreen}
          options={{
            tabBarIcon:({color,size}) => (
             <MaterialCommunityIcons name="account" color={color} size={size} />
            )
        }}
       />
         <Tab.Screen name="Home" component={HomeScreen}
       options={{
           tabBarIcon:({color,size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
           )
       }}
      />
      <Tab.Screen name="Keluar" component={Exit}
      options={{
        tabBarIcon:({color,size}) => (
         <MaterialCommunityIcons name="exit-to-app" color={color} size={size} />
        ),
        tabBarVisible:false,
    }}
      />
    </Tab.Navigator>
)

export default class App extends React.Component {
  
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="LoginScreen" 
         screenOptions={{
          headerTintColor: 'white',
          headerStyle: { backgroundColor: '#3891B7' },
          headerTitleAlign:"center"
        }}
        >
          <Stack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={Tabscreen} options={{ headerShown: false }} />
          <Stack.Screen name='DetailDunia' component={DetailScreenDunia} options={{ headerShown: true, title:'Detail [Dunia]' }} />
          <Stack.Screen name='DetailIndo' component={DetailScreenIndo} options={{ headerShown: true, title:'Detail [Indonesia]' }} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}