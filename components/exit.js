import { StatusBar } from 'expo-status-bar';
import React, { BackHandler } from 'react-native';
import { StyleSheet, Text, View } from 'react-native';

export default function exit() {
  return (
    <View>
        {BackHandler.exitApp()}
    </View>
  );
}