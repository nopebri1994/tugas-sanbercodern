import { createStore } from 'redux'
import { reducer } from './Reducer'

// Mendefinisikan store menggunakan reducer
const store = createStore(reducer)

export default store