import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,TextInput,TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
    this.loginHandler = this.loginHandler.bind(this);
  }

  loginHandler() {
    if(this.state.password == '12345678'){
      this.setState({isError:false})
      this.props.navigation.navigate('Home', {
        screen: 'Home',
        params: { userName: this.state.userName },
      });
    }else{
      this.setState({isError:true})
    }
  }

  render(){
  return (
        <View style={styles.container}>
            <View style={styles.boxtitle}>
                    <Text style={styles.textHead1}>Pantau</Text>
                    <Text style={styles.textHead2}>Corona</Text>
                    <Text style={styles.textHead3}>Login</Text>
                    <View style={{alignItems:"center"}}>
                        <View style={styles.bulat}>
                            <FontAwesome name="user-circle" size={67} color="black" />
                        </View>
                    </View>
            </View>
            <View style={styles.boxBody}>
                <View>
                    <Text style={{paddingLeft:60}}>Username ... </Text>
                    <View style={{alignItems:"center"}}>
                        <TextInput
                                style={styles.textInput}
                                placeholder='Masukkan Nama User/Email'
                                onChangeText={userName => this.setState({ userName })}
                            />
                    </View>
                </View>
                <View>
                    <Text style={{paddingLeft:60}}>Password ... </Text>
                    <View style={{alignItems:"center"}}>
                        <TextInput
                                style={styles.textInput}
                                placeholder='Masukan Password'
                                onChangeText={password => this.setState({ password })}
                                secureTextEntry={true}
                            />
                    </View>
                </View>
                <View>
                    <View style={{alignItems:"center"}} >
                    <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
                         <TouchableOpacity onPress={() => this.loginHandler()}>
                             <View style={styles.button}>
                                 <Text style={styles.buttonText}>Masuk</Text>
                             </View>
                         </TouchableOpacity>
                    </View>
                </View>
                <View>
                    <View style={{alignItems:"center"}}>
                           <Text style={styles.buttonText2}>Atau</Text>                       
                    </View>
                </View>
                <View>
                    <View style={{alignItems:"center"}}>
                         <TouchableOpacity>
                             <View style={styles.button}>
                                 <Text style={styles.buttonText}>Daftar</Text>
                             </View>
                         </TouchableOpacity>
                    </View>
                </View>
            </View>
            
        </View>
  );
}}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop:25
  },
  boxtitle:{
    width:360,
    height:229,
    backgroundColor:'#3891B7',
    elevation:20,
  },
  textHead1 : {
    color:'#000000',
    fontSize:36,
    fontWeight:'bold',
    paddingLeft:69,
    paddingTop:42,
  },
  textHead2 : {
    color:'#000000',
    fontSize:36,
    fontWeight:'bold',
    paddingLeft:173,
  },
  textHead3 : {
    color:'#fff',
    fontSize:24,
    fontWeight:'bold',
    textAlign:"center",
    paddingTop:20,
  },
  bulat:{
      margin:20,
      alignItems:'center',
      backgroundColor:'#fff',
      width:67,
      borderRadius:35,
      elevation:2,
  },
  boxBody:{
    paddingTop:50,
    height:350,
    justifyContent:"space-around",
  },
  textInput: {
    width: 256,
    height:40,
    backgroundColor: '#F8F4F4',
    borderWidth: 0.5,
    borderColor: "#9D9898",
    borderRadius:15,
    padding:5,

  },
  button:{
      height:37,
      width:137,
      backgroundColor:'#88928A',
      borderRadius:25,
      justifyContent:"center",
  },
  buttonText:{
    textAlign:'center',
    color:'white',
    fontWeight:"bold",
    fontSize:14,
  },
  buttonText2:{
    textAlign:'center',
    color:'#726967',
    fontWeight:"bold",
    fontSize:14,
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});
