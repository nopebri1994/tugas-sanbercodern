import { StatusBar } from 'expo-status-bar';
import React,{ Component } from 'react';
import { StyleSheet, Text, View, ActivityIndicator,TouchableOpacity} from 'react-native';
import Axios from 'axios';


export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          dataIndo: {},
          dataDuniaPos: {},
          dataDuniaSem: {},
          dataDuniaMen: {},
          isLoading: true,
          isError: false,
        };
      }
    
      // Mount User Method
      componentDidMount() {
        this.getKawalCorona()
      }
    
      //   Get Api Users
      getKawalCorona = async () => {
        try {
          const responseIndo = await Axios.get(`https://api.kawalcorona.com/indonesia`)
          const responseDuniaPos = await Axios.get(`https://api.kawalcorona.com/positif`)
          const responseDuniaSem = await Axios.get(`https://api.kawalcorona.com/meninggal`)
          const responseDuniaMen = await Axios.get(`https://api.kawalcorona.com/sembuh`)
             this.setState({ isError: false, 
                            isLoading: false, 
                            dataIndo: responseIndo.data,
                            dataDuniaPos:responseDuniaPos.data,
                            dataDuniaSem:responseDuniaSem.data,
                            dataDuniaMen:responseDuniaMen.data,
             })
        } catch (error) {
             this.setState({ isLoading: false, isError: true })
        }
      }    
  render(){
    if (this.state.isLoading) {
        return (
          <View
            style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
          >
            <ActivityIndicator size='large' color='blue' />
          </View>
        )
      }

      else if (this.state.isError) {
        return (
          <View
            style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
          >
            <Text>Terjadi Error Saat Memuat Data</Text>
          </View>
        )
      }
      // If data finish load
  return (
    <View style={styles.container}>
        <View style={styles.header}>
            <Text style={styles.TextHeader}>Pantau Covid - 19</Text>
        </View>
        <View style={styles.bodyHead}>
          <Text>Selamat Datang : {this.props.route.params.userName}</Text>
            <View style={styles.garis}></View>
        </View>
        <View style={{alignItems:"center"}}>
        <TouchableOpacity onPress={() =>  this.props.navigation.navigate('DetailDunia', {
                                          dataKirim:'Dunia',
                                        })}>
            <View style={styles.box}>
                <View style={{alignItems:"center"}}>
                    <Text style={styles.boxHead}>Data Dunia</Text>
                </View>
                <View>
                     <Text style={styles.boxChild}>Kasus Positif  { this.state.dataDuniaPos.value } Orang</Text>
                     <Text style={styles.boxChild}>Kasus Sembuh  { this.state.dataDuniaSem.value } Orang</Text>
                     <Text style={styles.boxChild}>Kasus Meninggal  { this.state.dataDuniaMen.value } Orang</Text>
                </View>
            </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() =>  this.props.navigation.navigate('DetailIndo', {
                                          dataKirim:'Indonesia',
                                        })}>
            <View style={styles.box}>
                <View style={{alignItems:"center"}}>
                    <Text style={styles.boxHead}>Data  { this.state.dataIndo[0].name }</Text>
                </View>
                <View>
                     <Text style={styles.boxChild}>Kasus Positif  { this.state.dataIndo[0].positif } Orang</Text>
                     <Text style={styles.boxChild}>Kasus Sembuh  { this.state.dataIndo[0].sembuh } Orang</Text>
                     <Text style={styles.boxChild}>Kasus Meninggal  { this.state.dataIndo[0].meninggal } Orang</Text>
                </View>
            </View>
            </TouchableOpacity>
        </View>
    </View>
  );
}}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      paddingTop:25
    },
    header:{
        width:360,
        height:60,
        backgroundColor:'#3891B7',
        alignItems:"center",
        justifyContent:"center",
    },
    TextHeader:{
    fontSize:24,
    fontWeight:"bold",
    color:'white',
    },
    bodyHead:{
        margin:20,

    },
    garis:{
        borderWidth:1,
    },
    box:{
        backgroundColor:'#fff',
        height:150,
        borderColor:'#000',
        borderWidth:0.5,
        width:330,
        borderRadius:15,
        elevation:5,
        margin : 10,
    },
    boxHead:{
        fontWeight:"bold",
        fontSize:18,
    },
    boxChild:{
        fontWeight:"bold",
        fontSize:14,
        margin:10,
    }
})