
export const types = {
    ADD: 'ADD',
    REMOVE: 'REMOVE',
  }
  
  export const actionCreators = {
    add: item => {
      return { type: types.ADD, payload: item }
    },
    remove: index => {
      return { type: types.REMOVE, payload: index }
    },
  }
  
  // Initial state dari store
  const initialState = {
    login: [],
  }

  export const reducer = (state = initialState, action) => {
    const { login } = state
    const { type, payload } = action
  
    switch (type) {
      case types.ADD: {
        return {
          ...state,
          todos: [payload, ...login],
        }
      }
      case types.REMOVE: {
        return {
          ...state,
          login: login.filter((login, i) => i !== payload),
        }
      }
    }
  
    return state
  }