import { StatusBar } from 'expo-status-bar';
import React,{ Component } from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList} from 'react-native';
import Axios from 'axios';

export default class AboutScreen extends Component {
    constructor(props) {
        super(props)
    }
  render(){

  return (
    <View style={styles.container}>
        <View style={styles.header}>
            <Text style={styles.TextHeader}>Pantau Covid - 19</Text>
        </View>
        <View style={styles.box}>
                <View style={{alignItems:"center"}}>
                    <Text style={styles.boxHead}>Tentang Aplikasi</Text>
                    <View style={styles.garis}></View>
                </View>
                <View  style={{margin:13}}>
             <Text>
                Aplikasi ini dibuat sebagai syarat untuk mendapatkan sertifikan pelatihan sanbercode batch juni 2020. 
             </Text>
             <Text>
                Aplikasi ini dibuat dengan menggunakan API dari kawalcorona.com yang juga sangat membantu dalam penyelsesaian tugas ini.
             </Text>
             <Text>
               Saya ucapkan banyak terima kasih kepada sanbercode selaku penyelenggara dan juga kepada seluruh instruktur yang membantu dalam proses
               bootcamp online kali ini.
             </Text>
             <Text>
              Terima Kasih.
             </Text>
                </View>
            </View>
    </View>
  );
}}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      paddingTop:25,
      alignItems:"center",
    },
    header:{
        width:360,
        height:60,
        backgroundColor:'#3891B7',
        alignItems:"center",
        justifyContent:"center",
    },
    TextHeader:{
    fontSize:24,
    fontWeight:"bold",
    color:'white',
    },
    box:{
      backgroundColor:'#fff',
      height:500,
      borderColor:'#000',
      borderWidth:0.5,
      width:330,
      borderRadius:15,
      elevation:5,
      margin : 10,
  },
  boxHead:{
      fontWeight:"bold",
      fontSize:18,
  }, 
   garis:{
     width:300,
    borderWidth:0.5,
},
})