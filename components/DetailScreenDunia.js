import { StatusBar } from 'expo-status-bar';
import React,{ Component } from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, SafeAreaView} from 'react-native';
import Axios from 'axios';

export default class DetailScreenDunia extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false,
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getKawalCorona()
  }

  //   Get Api Users
  getKawalCorona = async () => {
    try {
      const response = await Axios.get(`https://api.kawalcorona.com/`)
         this.setState({ isError: false, 
                        isLoading: false, 
                        data: response.data,
         })
         
    } catch (error) {
         this.setState({ isLoading: false, isError: true })
    }
  }    
render(){
if (this.state.isLoading) {
    return (
      <View
        style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
      >
        <ActivityIndicator size='large' color='blue' />
      </View>
    )
  }

  else if (this.state.isError) {
    return (
      <View
        style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
      >
        <Text>Terjadi Error Saat Memuat Data</Text>
      </View>
    )
  }
  return (
    <SafeAreaView style={styles.container}>
    <FlatList
      data={this.state.data}
      renderItem={({ item }) =>
       <View style={styles.box}>
         <View style={{alignItems:"center"}}>
             <Text style={styles.boxHead}>{item.attributes.Country_Region}</Text>
             <View style={styles.garis}></View>
         </View>
         <View>
              <Text style={styles.boxChild}>Kasus Positif  {item.attributes.Confirmed} Orang</Text>
              <Text style={styles.boxChild}>Kasus Sembuh  {item.attributes.Recovered} Orang</Text>
              <Text style={styles.boxChild}>Kasus Meninggal  {item.attributes.Deaths} Orang</Text>
         </View>
     </View>
      }
      keyExtractor={(item, index) => index.toString()}
    />
    </SafeAreaView>
  );
}}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems:"center"
    },
    header:{
        width:360,
        height:60,
        backgroundColor:'#3891B7',
        alignItems:"center",
        justifyContent:"center",
    },
    TextHeader:{
    fontSize:24,
    fontWeight:"bold",
    color:'white',
    },
    bodyHead:{
        margin:20,

    },
    garis:{
        borderWidth:0.5,
        width:300,
    },
    box:{
        backgroundColor:'#fff',
        height:150,
        borderColor:'#000',
        borderWidth:0.5,
        width:330,
        borderRadius:15,
        elevation:5,
        margin : 10,
    },
    boxHead:{
        fontWeight:"bold",
        fontSize:18,
    },
    boxChild:{
        fontWeight:"bold",
        fontSize:14,
        margin:10,
    }
})